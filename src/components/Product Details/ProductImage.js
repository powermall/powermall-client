import React, { useState } from "react";
import "swiper/components/navigation/navigation.min.css";
import "swiper/components/thumbs/thumbs.min.css";
import SwiperCore, { Navigation, Thumbs } from "swiper/core";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper.min.css";
SwiperCore.use([Navigation, Thumbs]);

const ProductImage = ({ img, ytLink }) => {
  const [thumbsSwiper, setThumbsSwiper] = useState(null);
  const image = img?.split(",");

  return (
    <div>
      <Swiper
        style={{
          "--swiper-navigation-color": "#fff",
          "--swiper-pagination-color": "#fff",
        }}
        spaceBetween={10}
        navigation={true}
        thumbs={{ swiper: thumbsSwiper }}
        className="mySwiper2"
      >
        {image?.map((img, i) => (
          <SwiperSlide key={i}>
            <img
              src={img}
              className="object-fill w-full mx-auto"
              alt="nature"
            />
          </SwiperSlide>
        ))}

        {ytLink && (
          <SwiperSlide>
            <iframe
              width="450"
              height="450"
              src={ytLink}
              title="YouTube video player"
              frameborder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
          </SwiperSlide>
        )}
      </Swiper>
      <Swiper
        onSwiper={setThumbsSwiper}
        spaceBetween={10}
        slidesPerView={4}
        watchSlidesProgress={true}
        className="mySwiper"
      >
        {image?.map((img, i) => (
          <SwiperSlide key={i}>
            <img
              src={img}
              className="object-contain w-full mx-auto"
              alt="nature"
            />
          </SwiperSlide>
        ))}
        {ytLink && (
          <SwiperSlide>
            <iframe
              width="100"
              height="100"
              src={ytLink}
              title="YouTube video player"
              frameborder="0"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
              allowfullscreen
            ></iframe>
          </SwiperSlide>
        )}
      </Swiper>
    </div>
  );
};

export default ProductImage;
